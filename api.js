const uuid = require('uuid/v4');
const express = require('express');
const router = express.Router();

module.exports = (todos) => {

    router.post('/todos', (req, res) => {
        const todo = req.body;
        todo.id = uuid();
        todos.push(todo)
        res.redirect('/')
    });
    
    router.route('/todo/:id')
        .put((req, res) => {
            const newTodo = req.body;
            const { id } = req.params;
            const oldTodoIndex = todos.findIndex(todo => todo.id === id);

            todos.splice(oldTodoIndex, 1, {...newTodo, ...id})
            res.status(200).json({
                message: 'todo is successfully edited'
            })
        })
        .delete((req, res) => {
            const { id } = req.params;
            const todoIndex = todos.findIndex(todo => todo.id === id);

            todos.splice(todoIndex, 1)
            res.status(200).json({
                message: 'todo is successfully deleted'
            })
        })

    return router
}