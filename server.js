const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const config = require('./config');
const { getViewPath } = require('./helpers');

// api router
const api = require('./api');

global.todos = [];

const app = express();

app.set('view engine', 'pug');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());

app.use('/assets', express.static(path.join(__dirname, 'assets')))

app.use(`${config.api}`, api(todos));


// render views
app.get('/', (req, res) => {
    res.render(getViewPath('todos'), todos)
});

app.get('/edit/:id', (req, res) => {
    const { id } = req.params;
    const todo = todos.find(todo => todo.id === id);

    res.render(getViewPath('edit'), todo)
})


app.listen(config.port, () => console.log(`App listening on port ${config.port}!`));