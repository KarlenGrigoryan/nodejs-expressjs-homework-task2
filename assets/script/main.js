function editTodo(id) {
    const name = document.querySelector('.todo-name').value
        const data = {
            id,
            name
        }
        fetch(`/api/todo/${id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((res) => {
            window.location.replace('/')
        })
}

function deleteTodo(id, elIndex) {
    fetch(`/api/todo/${id}`, {
        method: 'DELETE'
    }).then(() => {
        const todoEl = document.querySelector(`.todo-${elIndex}`);
        todoEl.remove()
    })
}
