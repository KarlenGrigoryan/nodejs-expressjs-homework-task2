module.exports = {
    port: process.env.PORT || 3001,
    api: '/api'
}